{include file="%s/templates/admin/_head.tpl"|args:$plugin_root title="Plugin Skin dense" current="plugin_%s"|args:$plugin.id}


<p>Tu peux naviguer dans le menu pour utiliser les vues personnalisées de ce skin.</p>
<br>
<p>Tant que tu es sur une vue du plugin, « Skin dense » est sélectionné dans le menu en plus, dès qu'il ne l'est plus alors tu es sur une page standard de Garradin ; dans ce cas, reviens sur cette page pour à nouveau naviguer dans les vues du plugin.</p>
<br>
<p>Les vues du skin sont signalées dans le menu par 👉</p>